<?php


namespace App\Controller\webservice;


use Doctrine\ORM\EntityManagerInterface;
use App\Service\Params;
use Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class WebServiceController extends AbstractController
{

    const FORMAT_JSON = 'json';
    const CONTENT_TYPE = 'Content-Type';
    const CONTENT_TYPE_VALUE = 'application/json';


    public $entityManager;
    public $statusCode;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function createSuccessResponse($entity){
        //array_push($groups, Params::LIST);
        $serializer = SerializerBuilder::create()->build();
        $data = $serializer->serialize($entity,self::FORMAT_JSON);
        $response = new Response($data);
        $response->headers->set(self::CONTENT_TYPE,self::CONTENT_TYPE_VALUE);
        return $response;
    }

    public function createErrorResponse($statusCode){
        return new Response( "",$statusCode);
    }

    public function savedFile($name,$path)
    {

    }

}