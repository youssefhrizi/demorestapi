<?php


namespace App\Controller\webservice;

use App\Entity\Author;
use App\Entity\Category;
use JMS\Serializer\SerializerBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;


/**
 * @Route("/api", name="api_category_")
 */
class CategoryController extends WebServiceController
{

    /**
     * List of all Category
     *
     * @Route("/category", name="index", methods={"GET"})
     */
    public function index()
    {
        $category = $this->entityManager->getRepository(Category::class)->findAll();
        return $this->createSuccessResponse($category);
    }

    /**
     * Show Category by ID
     *
     * @Route("/category/{id}", name="show_category_by_id", methods={"GET"})
     * @param int|null $id
     * @return Response
     */
    public function show(int $id = null)
    {
        $category = $this->entityManager->getRepository(Category::class)->find($id);
        return $this->createSuccessResponse($category);
    }

    /**
     *
     * @Route("/category", name="create_category", methods={"POST"})
     * @param resuest
     * @return Response
     */
    public function create(Request $request)
    {
        $category = new Category();
        $category->setName($request->request->get('name'));
        $this->entityManager->persist($category);
        $this->entityManager->flush();
        //return new Response("Cathegory created!",Response::HTTP_CREATED);
        return $this->redirectToRoute('api_category_show_category_by_id',['id'=>$category->getId()]);

    }

    /**
     * Delete Category
     * @Route("/category/{id}", name="delete", methods={"DELETE"})
     * @param int|null $id
     * @return Response
     */
    public function delete(int $id = null)
    {
        $category = $this->entityManager->getRepository(Category::class)->find($id);
        if(isset($category))
        {
            $this->entityManager->remove($category);
            $this->entityManager->flush();
            return $this->createSuccessResponse($category);
        }else
            {
                return $this->createErrorResponse(Response::HTTP_NOT_FOUND);
            }



    }
}