<?php


namespace App\Controller\webservice;


use App\Service;
use App\Entity\Files;
use App\Service\Params;
use App\Entity\Product;
use Symfony\Component\Asset\Package;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;




/**
 * @Route("/api", name="api_files_")
 */
class FilesController extends WebServiceController
{
    /**
     * @Route("/files", name="index", methods={"GET"})
     *
     */
    public function index()
    {
        $allFiles= $this->entityManager->getRepository(Files::class)->findAll();
        return $this->createSuccessResponse($allFiles);
    }

    /**
     * @Route("/files/{id}", name="show_by_id", methods={"GET"})
     * @param int|null $id
     * @return Response
     */
    public function showById(int $id = null)
    {
        $file= $this->entityManager->getRepository(Files::class)->find($id);
        return $this->createSuccessResponse($file);
    }

    /**
     * @Route("/files/ByName/{name}", name="show_by_name", methods={"GET"})
     * @param $name
     * @return Response
     */
    public function showByName($name)
    {
        $file= $this->entityManager->getRepository(Files::class)->findByName($name);
        return $this->createSuccessResponse($file);
    }

    /**
     * @Route("/files",name="create_files", methods={"POST"})
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $products = $this->entityManager->getRepository(Product::class)->find($request->request->get('product_id'));
        $uploadedFile = $request->files->get('myFile');

        //dump($request); die;
        // Random Name File
        $generateFileName = md5 ( uniqid () ) . '.' . $uploadedFile->getClientOriginalExtension();
        // Move File to the Directory
        $uploadedFile->move($this->getParameter('file_directory'),$generateFileName);
        // Send data to the Database
        $file = new Files();
        $file->setName($generateFileName);
        // just the name
        $file->setPath($generateFileName);
        $file->setProduct($products);
        $this->entityManager->persist($file);
        $this->entityManager->flush();
        // return response
        $message = "Files Created : ".$generateFileName;
        return new Response($message,Response::HTTP_CREATED);

    }
    /**
     * @Route("/files/{id}", name="update_files", methods={"POST"})
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $files = $this->entityManager->getRepository(Files::class)->find($id);
        $files->setName($request->request->get('name') ?? $files->getName());
        $files->setPath($request->request->get('path') ?? $files->getPath());

        $products = $this->entityManager->getRepository(Product::class)->find($request->request->get('product_id'));
        $files->setProduct($products);
        $this->entityManager->persist($files);
        $this->entityManager->flush();
        return new Response ('File Updated!',Response::HTTP_CREATED);

    }
    /**
     * @Route("/files/{id}", name="delete_files", methods={"DELETE"})
     */
    public function delete($id)
    {
        $files = $this->entityManager->getRepository(Files::class)->find($id);
        $this->entityManager->remove($files);
        $this->entityManager->flush();
        return new Response('File Deleted!',Response::HTTP_CREATED);
    }
}