<?php


namespace App\Controller\webservice;

use App\Entity\Author;
use JMS\Serializer\SerializerBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


    /**
     * @Route("/api", name="api_authors_")
     */
class AuthorController extends WebServiceController
{

    /**
     * List of all Authors
     *
     * @Route("/authors", name="index", methods={"GET"})
     */
    public function index()
    {
        $authors = $this->entityManager->getRepository(Author::class)->findAll();
        return $this->createSuccessResponse($authors);
    }

    /**
     *
     * @Route("/authors", name="create_authors", methods={"POST"})
     * @param resuest
     * @return Response
     */
    public function create(Request $request)
    {
        $author = new Author();
        $author->setFullname($request->request->get('fullname'));
        $author->setBiography($request->request->get('biography'));

        $this->entityManager->persist($author);
        $this->entityManager->flush();

        return new Response("Author created!",Response::HTTP_CREATED);

    }
}