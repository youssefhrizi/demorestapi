<?php


namespace App\Controller\webservice;


use JMS\Serializer\SerializerBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Product;
use App\Entity\Category;

use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/api", name="api_product_")
 */
class ProductController extends WebServiceController
{

    /**
     * List of all Products
     *
     * @Route("/products", name="index", methods={"GET"})
     */
    public function index()
    {
        $products = $this->entityManager->getRepository(Product::class)->findAll();
        return $this->createSuccessResponse($products);
    }

    /**
     * Show Products By ID
     *
     * @Route("/products/{id}", name="show_products_by_id", methods={"GET"}, requirements={"id"="\d+"})
     *
     * @return Response
     */
    public function show(int $id = null )
    {
        $product = $this->entityManager->getRepository(Product::class)->find($id);
        if(isset($product))
        {
            return $this->createSuccessResponse($product);

        }else{
            return $this->createErrorResponse(Response::HTTP_NO_CONTENT);
        }
    }

    /**
     * Show Products By ID
     *
     * @Route("/products/ByName/{name}", name="show_by_name", methods={"GET"})
     *
     * @param $name
     * @return Response
     */
    public function showByName($name)
    {
        $product = $this->entityManager->getRepository(Product::class)->findByName($name);
        if(isset($product))
        {
            return $this->createSuccessResponse($product);
        }else{
            return $this->createErrorResponse(Response::HTTP_NO_CONTENT);
        }
    }

    /**
     * Show by Products by Category
     *
     * @Route("/products/ByCategory/{name}", name="show_by_category", methods={"GET"})
     *
     * @return Response
     */
    public function showByCategory($name)
    {
        $product =$this->entityManager->getRepository(Product::class)->findByCategory($name);
        return $this->createSuccessResponse($product);
    }


    /**
     *  Add Product
     *
     * @Route("/products", name="create_products", methods={"POST"})
     *
     * @return Response
     */
    public function create(Request $request, ValidatorInterface $validator)
    {

      // Get Product
      $product = new Product();

      // Get Category
      $categoryRepository = $this->entityManager->getRepository(Category::class);
      $category = $categoryRepository->find($request->request->get('category_id'));
      $product->setName($request->request->get('name'));
      $product->setPrice($request->request->get('price'));
      $product->setBarCode($request->request->get('bar_code'));
      $product->setMarque($request->request->get('marque'));
      // relates this product to the category
      $product->setCategory($category);
      $this->entityManager->persist($product);
      $this->entityManager->flush();

      $error = $validator->validate($product);
      if(count($error)>0)
      {
          $errorString = (string) $error;
          return new Response($errorString);
      }

      //return new Response('Produit Ajouter avec success !',Response::HTTP_CREATED);
      return $this->redirectToRoute('api_product_show_products_by_id',['id'=>$product->getId()]);
    }

    /**
     * Update Product
     *
     * @Route("/products/{id}", name="update", methods={"POST"})
     *
     * @param Request $request
     * @param int|null $id
     * @return Response
     */
    public function update(Request $request ,int $id=null)
    {
        $product = $this->entityManager->getRepository(Product::class)->find($id);
        $product->setName($request->request->get('name') ?? $product->getName());
        $product->setPrice($request->request->get('price')?? $product->getPrice());
        $product->setBarCode($request->request->get('bar_code')?? $product->getBarCode());
        $product->setMarque($request->request->get('marque')?? $product->getMarque());
        $this->entityManager->persist($product);
        $this->entityManager->flush();
        return new Response('Produit modifier avec success !',Response::HTTP_CREATED);
    }

    /**
     * Delete Product
     *
     * @Route("/products/{id}", name="delete", methods={"DELETE"})
     *
     * @return Response
     */
    public function delete(int $id)
    {
        $product = $this->entityManager->getRepository(Product::class)->find($id);
        if(isset($product)){
            $this->entityManager->remove($product);
            $this->entityManager->flush();
            return new Response('Produit supprimer avec success !',Response::HTTP_CREATED);
        }else{
            return $this->createErrorResponse(Response::HTTP_NOT_FOUND);
        }

    }


}