<?php


namespace App\Controller\webservice;
use App\Entity\Article;
use App\Entity\Author;
use JMS\Serializer\SerializerBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

    /**
     * @Route("/api", name="api_articles_")
     */
class ArticleController extends WebServiceController
{

    /**
     *
     * @Route("/articles", name="show_articles", methods={"GET"})
     */
    public function index()
    {
        $articles = $this->entityManager->getRepository(Article::class)->findAll();
        return $this->createSuccessResponse($articles);
    }
    /**
     *
     * @Route("/articles", name="create_articles", methods={"POST"})
     * @param resuest
     * @return Response
     */
    public function create(Request $request)
    {
        $article = new Article();
        $entityRepository = $this->entityManager->getRepository(Author::class);
        $author=$entityRepository->find($request->request->get('author_id'));

        $article->setTitle($request->request->get('title'));
        $article->setContent($request->request->get('content'));
        $article->setAuthor($author);

        $this->entityManager->persist($article);
        $this->entityManager->flush();

        return new Response("Aricle created!",Response::HTTP_CREATED);

    }
}