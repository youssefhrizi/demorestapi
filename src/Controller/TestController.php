<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;




class TestController extends AbstractController
{


    public function index()
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/TestController.php',
            'products' => [
                'id' => 5,
                'label' => 'coucou'
            ]
        ]);


    }

}